#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys

sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://blog.mtmxr.com'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'atom.xml'
FEED_ALL_RSS = 'rss.xml'
CATEGORY_FEED_ATOM = 'category/%s/atom.xml'
CATEGORY_FEED_RSS = 'category/%s/rss.xml'
TAG_FEED_ATOM = 'tag/%s/atom.xml'
TAG_FEED_RSS = 'tag/%s/rss.xml'
RSS_FEED_SUMMARY_ONLY = False

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

DISQUS_SITENAME = 'mtmxr'
DISQUS_DISPLAY_COUNTS = True
DISQUS_ID_PREFIX_SLUG = True
# GOOGLE_ANALYTICS = ""
