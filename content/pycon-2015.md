Title: PyCon 2015
Date: 2015-04-19T20:33:39-0400
Category: events
Tags: conferences, python

I went to [PyCon 2015](https://us.pycon.org/2015/) in Montréal last week
and it was, in a word, awesome.

This was my first PyCon and the biggest free software conference I've
been to yet. So glad I went, although I only had enough time to go to
the main conference. There were a few tutorials before the conference
that sounded interesting, and a lot I could have worked on during the
sprints. But as it is I had three days full of excellent talks and got
some ideas and inspiration for future projects.

<!-- PELICAN_END_SUMMARY -->

Out of all of the fascinating talks I listened to, I'll just describe a
few of the highlights. David Beazley gave a really insightful
demonstration on performance tradeoffs when dealing with
[concurrency](http://pyvideo.org/pycon-us-2015/python-concurrency-from-the-ground-up-live)
in Python programs. Allison Kaptur delivered back-to-back talks on
[bytecode](http://pyvideo.org/pycon-us-2015/bytes-in-the-machine-inside-the-cpython-interpre)
and on tips for
[reading and studying source code](http://pyvideo.org/pycon-us-2015/exploring-is-never-boring-understanding-cpython).
There was a good review of how
[names and values](http://pyvideo.org/pycon-us-2015/facts-and-myths-about-python-names-and-values)
are handled in Python by Ned Batchelder. And Josh Triplett gave a really
cool talk and demonstration on his work on
[making Python work without an operating system](http://pyvideo.org/pycon-us-2015/porting-python-to-run-without-an-os).
I highly recommend giving all of these a watch. There were many more
interesting talks I went to at the conference, and many more that I had to
miss. Fortunately, all of the conference talks, tutorials, and keynotes are
recorded and freely available on the
[pyvideo.org](http://pyvideo.org/events/pycon-us-2015) site.

There's also of course the feeling of community, collaboration, and
inspiration from being at a conference like this with thousands of other
Python users and developers and seeing everyone's diverse projects and
ideas coming together. I hope to keep the energy and ideas I've gotten
going for my own projects.

So with this conference over, I intend to catch up on tutorials and
talks I missed, start doing some great work with Python, and hopefully
make it to PyCon again next year.
