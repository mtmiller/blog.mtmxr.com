Title: Hello, World!
Date: 2014-05-18T19:14:47-0400
Category: personal

Look, someone's starting a new blog on the internet. How original. Why should
you read this one? You probably shouldn't. But here's why I'm writing it
anyway.

<!-- PELICAN_END_SUMMARY -->

Sometimes we just need to do something completely different. Maybe we're
languishing in a routine and need a change of scenery. Maybe we've hit a
creative roadblock and a break and some other activity are needed to help clear
the mental pathways. Maybe we find ourselves burned out and not getting
anywhere for whatever reason despite our best efforts. Whatever the cause,
sometimes it's best to stop and try something else for a while.

On that note, now is the perfect time for me to start this blog.

I have considered keeping a blog on and off for a long time, but there was
always something else to be done first[^1]. Starting is the hardest part, so
with that out of the way now, hopefully it will become a regular thing.

So, what am I going to blog about?

Most likely a lot about software, since that is what I spend most of my waking
hours doing and thinking about. Probably some other things, too. Only time will
tell what this project may turn into. I might even get some readers along the
way. That's where you come in. Let's see what happens.

[^1]: Anyone who knows me well should recognize this familiar pattern.
