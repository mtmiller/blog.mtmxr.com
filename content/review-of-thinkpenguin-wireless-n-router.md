Title: Review of ThinkPenguin Wireless N Router
Date: 2015-08-06T22:43:52-0400
Category: reviews
Tags: fsf, hardware, review, wifi

This is a review of the [ThinkPenguin Wireless N Router][router] that I
recently purchased for my home network. This router is sold by
[ThinkPenguin][thinkpenguin], a small maker of computers and equipment
all factory-installed with free software, and is certified to
[respect my freedom][ryf], so why not give it a try?

<!-- PELICAN_END_SUMMARY -->

This router had been on my radar for several months now, ever since
hearing about it from the FSF. I didn't buy it as soon as I heard of it,
because I honestly didn't need a new router at the time and I'm not one
to go out and buy gadgets without knowing I'll actually use them. A few
months passed and I took a harder look at it and thought it might be
time to replace my aging WRT54GS, but still held off. Then two days ago
my router suddenly stopped transmitting, and I immediately went to
ThinkPenguin.

The router comes in modest packaging straight from ThinkPenguin HQ. The
box includes the router, 9V power adapter, ethernet cable, quick start
one-pager, a couple of product brochures to hand out to your friends,
and yes, a CD with source code!

![Photo of ThinkPenguin Wireless N Router package contents][packaging]

Since I had no working home network, I just wanted to get this router
configured and working as quickly as possible. So I unboxed it, powered
it on and hooked it up to my laptop. The router comes with an actual
physical on/off button, and once it's turned on it seems to boot in
about 30 seconds.

The router runs [LibreCMC][librecmc], which is a GNU/Linux distribution
designed to be run on routers and other similar systems. Using the web
management software to log on and configure the router is super easy, as
expected. Within a couple of minutes I was able to set it up to provide
the same network SSID and password as my previous router and it was
essentially ready to use.

Just to be sure that the default configuration was okay, I briefly
scanned through all of the options shown in the web management
interface. The router comes with an SSH server and a firewall with
pretty sane defaults. The only services with open ports by default are
the web interface, SSH, DNS, and DHCP. I didn't see anything that really
needed to be changed before using it as is.

I'm now going back to my normally scheduled internet usage, I'll post an
update here when I've put some miles on the router.

[router]: https://www.thinkpenguin.com/gnu-linux/free-software-wireless-n-broadband-router-gnu-linux-tpe-nwifirouter2 "ThinkPenguin Wireless N Router"
[thinkpenguin]: https://www.thinkpenguin.com/ "ThinkPenguin"
[ryf]: https://fsf.org/ryf "Respects Your Freedom"
[librecmc]: https://librecmc.org/ "LibreCMC"
[packaging]: {filename}/images/thinkpenguin-router-unbox.jpg
