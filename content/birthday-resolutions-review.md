Title: Birthday Resolutions - Review
Date: 2015-05-20T19:33:36-0400
Category: personal
Tags: conferences, fitness, general, octave, self

Last year on my birthday I decided to try setting some goals for
self-improvement with a deadline of the following birthday. People
typically set New Year's resolutions for themselves, but I wanted to try
something different. Partly because I'm a natural contrarian, but also
because my birthday last year was unique and offered more than a few
reasons for self-reflection. So with another birthday looming, it's time
now to review how this experiment worked out.

<!-- PELICAN_END_SUMMARY -->

First, because this was a particularly notable birthday I had decided to
hold myself to 10 resolutions. So this was almost doomed to failure from
the beginning, if success means hitting all 10, which I didn't. If I
want to do this again next year, I should definitely go with a smaller
set of goals to better set myself up for success. Obvious.

Some of my goals were broader than others, which made it harder to
define a successful target to aim for. For example, my goal to attend
more free software developer conferences (did) was a lot easier to
define and complete than my goal to make more time for creative pursuits
(didn't).

Despite these problems, I like how this experiment turned out. I was
able to accomplish about half of my 10 goals (for some definition of
"accomplish"), and I'm not one to dwell on the other half that didn't
get done. I also like pinning personal goals to my birthday, rather than
arbitrarily to the start of the Western calendar year. It reminds me to
not only celebrate my birthday but to keep trying to improve from year
to year.

Anyone else tried this? Any other non-traditional ideas for annual, or
more frequent, resolutions and personal goals?
