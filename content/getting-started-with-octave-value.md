Title: Getting Started with octave_value
Date: 2015-10-24T03:54:04-0400
Category: development
Tags: code, octave, tutorial
Status: draft

Creating a custom type for Octave in C++ involves defining a new
octave_value. This is the wrapper for all matrices, classes, and other
types that are known to the interpreter. Adding a new octave_value is
done by defining a subclass of octave_base_value and registering it with
the type system. Sounds simple, let's look at a concrete example.

I recently started a small Octave project that needed to define a new
octave_value.
