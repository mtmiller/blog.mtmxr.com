Title: Octave + Python: A New Hope
Date: 2015-05-11T23:41:52-0400
Category: development
Tags: octave, python

As a fan of both Python and Octave for numerical computing, and an
active Octave developer, I'm always excited to hear about projects in
either environment that create new capabilities or open up new ways of
looking for solutions to problems. So I am especially excited about a
new project that has the potential to bring Octave and Python much
closer together and to give users of either tool full use of the other.

The broad goal of this project is to provide a two-way interface layer
between Octave and Python. What does this mean specifically?<!-- PELICAN_END_SUMMARY -->
Well, I expect a future version of Octave to have a function that will
call Python functions, using an embedded Python runtime, with
transparent conversion between native Octave types and Python / NumPy
types. There will also be a Python module to do the inverse: allow
Python code to call Octave functions, invoke an embedded Octave
interpreter, and have automatic conversion between Python and Octave
types.

The way in which the seeds of this project came together very quickly is
really interesting, and what I want to describe in this post. The first
was in a mailing list side discussion in late March about the
appropriateness of Octave and Matlab for teaching numerical programming.
It was [mentioned][rcrozier] that recent versions of Matlab have a
[calling interface to Python][matpy]. For years they had provided a
similar interface to Java, but I had no idea that Python was now an
option for Matlab users. I filed that away for later.

Then there is the [Octave symbolic package][octsympy], which relies
heavily on SymPy to do the actual symbolic computation, but interacts
with Python and SymPy over a pipe. So that existing package would
definitely benefit from having a Python interpreter embedded in Octave
or in a loadable oct-file.

And finally there was [a post][jordi] in early April from fellow Octave
developer JordiGH, who wrote:

> I have a wild idea. I like Python, and I think Numpy and Scipy are a
> great tool. Interfacing Scipy with Octave is also a good thing.
> ...
> I therefore propose to bring Pytave into Octave proper.

Pytave is an already-existing project which provides a Python module
that can call Octave functions. It worked with older versions of Octave
years ago, but has not kept up with the Octave API. It did work, and it
does have a lot of useful code for converting between Octave and Python
types, lots of good groundwork to start building from.

I'm not sure what led Jordi to think of this "wild idea" or share it
with us, but it definitely inspired me to latch onto this project. The
timing of his message, after the other previous uses and mentions of
Python, *and* being just days before the start of
[my first PyCon experience][pycon2015], read to me like a call to
action. This felt like a perfect confluence of events and ideas to bring
Octave and Python together in a novel way.

So, I have already put some effort into this, and am planning to do some
more. I hope that I (and any other interested contributors) will be able
to make some real progress on this Octave-Python interface during this
summer. I will share some more specifics about the project in a followup
post soon.

Thoughts about this project? Interested in following our progress or
contributing?

  [rcrozier]: https://lists.gnu.org/archive/html/octave-maintainers/2015-03/msg00351.html
  [matpy]: https://www.mathworks.com/help/matlab/call-python-libraries.html
  [octsympy]: https://github.com/cbm755/octsympy
  [jordi]: https://lists.gnu.org/archive/html/octave-maintainers/2015-04/msg00062.html
  [pycon2015]: {filename}pycon-2015.md
