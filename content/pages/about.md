Title: About

I am a free software developer and advocate and all around geek. This
blog will probably cover my free software projects, general software
development and computing topics, and whatever other miscellanous topics
interest me.

This is a personal blog. Except where otherwise explicitly noted, all
content is copyrighted under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
Any views or opinions represented in this blog are my own and do not
represent those of my employer.
