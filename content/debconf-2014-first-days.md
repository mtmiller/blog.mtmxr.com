Title: DebConf 2014 - First Days
Date: 2014-08-25T23:51:15-0700
Category: events
Tags: conferences, debian

The first three days of DebConf 2014 are over. This is my first DebConf
ever, being a long-time Debian user but only recently come into the
community as an active contributor, and now seems like a good
opportunity to take a few minutes and write about my experience here so
far.

First off, I'm not entirely sure what I was expecting DebConf to be or
what I was expecting to get out of it. I think for all conferences I've
been to, the most valuable thing has been seeing people talk about the
projects that matter to them and rediscovering why I use and work on
free software. So, certainly that, plus just being around and absorbing
as much *Debian* as possible, which is seriously lacking in my normal
circles.

<!-- PELICAN_END_SUMMARY -->

The first two days were full of high-level concept talks for me. This
felt like a good way to start the conference, before diving into days
and days of projects and technical details. Former DPL Stefano
Zacchiroli ([@zacchiro](https://twitter.com/zacchiro)) kicked things off
by talking about how free software does continue to be more and more
widely adopted, but why that may no longer be enough in an increasingly
cloud-centered world. To paraphrase one of his main points, if everyone
in the world is using completely free operating systems, does that even
matter if they just use a web browser to access proprietary web
applications? Excellent question.

Next, Biella Coleman
([@BiellaColeman](https://twitter.com/BiellaColeman)) gave a brief
glimpse into her research on the history of Anonymous. This was
definitely fascinating work, and I'm planning on snagging a copy of her
book on the topic when it comes out. The tie-in to Debian and free
software was a bit tenuous, but still an interesting talk.

I really enjoyed the personal journey that Christine Spang
([@spang](https://twitter.com/spang)) took us on as she described her
discovery of other geeks on the internet, the Debian community, and
where it all took her. I have to say her story really resonated with me
and my own history. It also helped me reflect on the opportunities I
have forgone and missed over the course of my comparatively much longer
journey to being involved in this community.

There was a discussion about how to improve collaboration between Debian
and the FSF. John Sullivan ([@johns_FSF](https://twitter.com/johns_FSF))
presented his view of the current state of affairs and areas where the
two projects have similar goals and could work more closely. Of course
there are many different opinions on what successful outcomes would look
like and even on what kinds of collaboration make sense. This is
something I have followed in the past and am deeply interested in, being
both a Debian Developer and a FSF member, so I will likely rewatch the
discussion once recordings are available and maybe write more about it
someday.

The third day was all about Debian internal organizational things. First
I attended a session with the Debian Technical Committee, where the
committee discussed their work over the past year, current work, and
opened the floor for questions and discussion. The best part of this
session for me was just seeing the real people behind the discussions.
After that, a discussion of the job of maintenance of the Debian
keyring, which is what lets maintainers do their work in Debian and
protects the integrity of the software archive. I am no crypto expert, I
know just enough to be dangerous, so I learned a lot about what the
keyring team does and what improvements they are hoping to make. There
was a lot of great feedback and suggestions, in particular documenting
best practices or recommended procedures for key creation and handling.
This was followed later in the afternoon with a large keysigning party.

I have not done much Debian work during DebConf yet. I was inspired by
the FSF discussion to reevaluate my limited use of proprietary software,
though, so I am now pleased to report that the only non-free packages I
use on Debian are documentation and hardware-related (drivers and
firmware). And I have now contributed (questionable value?) this
correspondence. Anyway, on with the rest of DebConf; I'll be following
this post with another after the conference ends.
