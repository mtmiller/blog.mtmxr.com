Title: Clarendon Day 5K 2014
Date: 2014-09-30T22:17:50-0400
Category: events
Tags: exercise, fitness, running

This past weekend, I ran the
[Clarendon Day 5K](https://runpacers.com/race/clarendon-day-10k-5k-kids-dash/)
and finished with a PR of 28:01. This was a really nice, straight,
mostly downhill, course through the heart of the Clarendon and Rosslyn
neighborhoods of Arlington, VA. This was my first time running this
course, and the opportunity to run down Wilson past so many places I
used to frequent when I lived in Arlington was one I couldn't pass up. I
don't get to be back in Arlington as much as I used to these days, so it
was a treat to be able to spend some quality time running down this
stretch of road. I also enjoy city running in general, going past office
buildings and hotels, restaurants and city parks. The 5K was followed by
a 10K, which I am not ready to take on, but many others were. Definitely
worthwhile, I will run it again next year if I can.

<!-- PELICAN_END_SUMMARY -->

This race was a big accomplishment for me. A 5K in 28 minutes, not that
big a deal I'll concede. The accomplishment is in the bigger context of
health and fitness as an integral part of my life. Exercise and
healthier eating have been ongoing themes for me in 2014, especially
since the beginning of June. This race caps off four months of focusing
on physical activity and nutrition every single day. Four months over
which I've turned myself back into a runner and shed 25 pounds along the
way. Four months over which I have started to move past prior setbacks
and defeats and try again to become a distance runner. And lastly, this
is the first race that I've been able to run and finish in over 4 years.
It went so well, I hope it will not be the last. I've got my eye on a
few more local races coming up this fall and winter, stay tuned for
updates on my progress.

![Mike at Clarendon Day 5K 2014]({filename}/images/mike-clarendon-day-5k-2014.jpg)
