#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Mike Miller'
SITENAME = 'Disassembling Mike'
SITEURL = ''
SITESUBTITLE = 'A blog about free software and thoughts.'

PATH = 'content'

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = 'en'

CC_LICENSE = 'CC-BY-SA'

THEME = 'pelican-themes/pelican-bootstrap3'
THEME_TEMPLATES_OVERRIDES = ['templates']

CUSTOM_CSS = 'static/custom.css'

JINJA_ENVIRONMENT = {'extensions': ('jinja2.ext.i18n',)}

PLUGIN_PATHS = ('pelican-plugins',)
PLUGINS = ('i18n_subsites', 'sitemap', 'summary')

DIRECT_TEMPLATES = ('index', 'tags', 'categories', 'archives')

STATIC_PATHS = ('images', 'extra')

EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
    'extra/robots.txt': {'path': 'robots.txt'},
}

IGNORE_FILES = ('.#*', '*~')

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = (
    ('Twitter', 'https://twitter.com/mtmiller'),
    ('GitLab', 'https://gitlab.com/mtmiller'),
    ('GitHub', 'https://github.com/mtmiller'),
    ('Bitbucket', 'https://bitbucket.org/mtmiller'),
    ('Stack Overflow', 'https://stackoverflow.com/users/384593/mike-miller'),
    ('Subscribe', '/atom.xml', 'rss'),
)

GITHUB_USER = 'mtmiller'
GITHUB_REPO_COUNT = 3
GITHUB_SKIP_FORK = True

TWITTER_CARDS = True
TWITTER_USERNAME = 'mtmiller'

DEFAULT_PAGINATION = 10

SITEMAP = {'format': 'xml'}

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# Set up desired URL permalink naming scheme

ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
DRAFT_URL = 'drafts/{slug}'
DRAFT_SAVE_AS = 'drafts/{slug}/index.html'
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = 'category/{slug}'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}/index.html'
AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''
ARCHIVES_URL = 'archives'
ARCHIVES_SAVE_AS = 'archives/index.html'
CATEGORIES_URL = 'categories'
CATEGORIES_SAVE_AS = 'categories/index.html'
TAGS_URL = 'tags'
TAGS_SAVE_AS = 'tags/index.html'

SLUG_SUBSTITUTIONS = (('+', 'plus'),)
